<?php
/**
 * Created by PhpStorm.
 * User: andrej
 * Date: 6/14/17
 * Time: 19:36
 */

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class ArticleAdmin extends AbstractAdmin  {

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper

            ->add('title')
            ->add('content', CKEditorType::class, array(

            ))
            ->add('status', 'choice', array(
                'label' => 'Zobraziť verejne',
                'choices' => array(1 => 'Ano', 0 => 'Nie'),
                'multiple' => false,
                'required' => true
            ))
            ->add('category')
            ->add('author')

            ->add('images', 'sonata_type_model', array(
                'by_reference' => true,
                'multiple' => true
            ))
        ;
    }
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('status')

        ;
    }


    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('title')
            ->add('status','boolean',array('editable' => true))
            ->add('author')
        ;
    }
}