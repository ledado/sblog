<?php
/**
 * Created by PhpStorm.
 * User: andrej
 * Date: 6/15/17
 * Time: 07:22
 */

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class ImageAdmin extends AbstractAdmin  {

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('file', 'file', array(
                'required' => false
            ))
            ->add('type', 'choice', array(
                'choices' => array(
                    '1' => 'Náhľad',
                    '2' => 'Galéria'
                )
            ))

        ;
    }
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper


        ;
    }


    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper

        ;
    }


    public function prePersist($image)
    {
        $this->manageFileUpload($image);
    }

    public function preUpdate($image)
    {
        $this->manageFileUpload($image);
    }

    private function manageFileUpload($image)
    {
        if ($image->getFile()) {

        }
    }

}