<?php
/**
 * Created by PhpStorm.
 * User: andrej
 * Date: 6/14/17
 * Time: 21:32
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MainController extends Controller
{
    const limit = 5;

    public function indexAction($page = 1){ //hlavna stranka
        $em = $this->get('doctrine')->getManager();

        if (!$page) {
            $page = 1;
        }

        $articles = $em->getRepository('AppBundle:Article')->findByPageAndLimit($page, $this::limit);

        $countArticles = $em->getRepository('AppBundle:Article')->getCount();

        $maxPages = ceil($countArticles / $this::limit);

        return $this->render('AppBundle:Main:index.html.twig',array(
            'articles' => $articles,
            'title' => 'Najnovšie články',
            'maxPages' => $maxPages,
            'thisPage' => $page));
    }

    public function showArticleAction($id){ //zobrazovanie nahladu clanku
        $em = $this->get('doctrine')->getManager();

        $article = $em->getRepository('AppBundle:Article')->findOneById($id);
        if($article){
            return $this->render('AppBundle:Main:article_detail.html.twig',array('article' => $article));
        }else{
            return $this->render('AppBundle:Main:not_found.html.twig');
        }
    }

    public function showCategoryAction($id, $page){ //zobrazovanie clankov v jednotlivych kategoriach
        $em = $this->get('doctrine')->getManager();

        $category = $em->getRepository('AppBundle:Category')->findOneById($id);

        if (!$page) {
            $page = 1;
        }

        if($category){

            $countArticles = $em->getRepository('AppBundle:Article')->getCount($id); //pocet clanok v danej kategorii

            $maxPages = ceil($countArticles / $this::limit);

            $articles = $em->getRepository('AppBundle:Article')->findByCategory($id, $page, $this::limit);

            return $this->render('AppBundle:Main:index.html.twig',array(
                'articles' => $articles,
                'title' => $category->getTitle(),
                'maxPages' => $maxPages,
                'thisPage' => $page,
                'categoryId' => $id));
        }else{
            return $this->render('AppBundle:Main:not_found.html.twig');
        }
    }


    public function showMenuAction($max = 8){ //zobrazovanie menu
        $em = $this->get('doctrine')->getManager();

        $categories = $em->getRepository('AppBundle:Category')->findAll($max);

        return $this->render('AppBundle:Component/Menu:menu.html.twig',array('categories' => $categories));

    }

    public function showMostPopularAction($max = 10){
        $em = $this->get('doctrine')->getManager();

        $articles = $em->getRepository('AppBundle:Article')->findMostPopular($max);

        return $this->render('AppBundle:Component/Article:most_popular_list.html.twig',array('articles' => $articles));
    }
}